#include <windows.h>
#include <Commctrl.h>
#include "time.h"

LPSTR NazwaKlasy = "Klasa Okna";
MSG Komunikat;
HWND hwnd;
int rozmiar = 6;
CRITICAL_SECTION SekcjaKrytyczna;
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	horizontal = desktop.right;
	vertical = desktop.bottom;
}
DWORD WINAPI ThreadFunc_update_CellState(LPVOID lpdwParam);
class Cell
{
	int x, y;
	DWORD dwThreadId;
public:
	HWND handle;
	bool isWorking;
	HANDLE ahThread;
	volatile int state;
	Cell()
	{
		state = rand() % 9 + 1;
		isWorking = 0;
	}
	~Cell()
	{
	}
	void fill(int wsp_x, int wsp_y)
	{
		x = wsp_x;
		y = wsp_y;
	}
	void start_thread()
	{
		ahThread = CreateThread(NULL, 0, ThreadFunc_update_CellState, (LPVOID)(x * rozmiar + y), 0, &dwThreadId);
	}
};
Cell **cell;

DWORD WINAPI ThreadFunc_update_CellState(LPVOID lpdwParam) 
{
	int x, y;
	int suma = 0;
	x = (int)lpdwParam / rozmiar;
	y = (int)lpdwParam % rozmiar;
	while (true)
	{
		EnterCriticalSection(&SekcjaKrytyczna);
		if (cell[(x + 1) % rozmiar][y].isWorking == 0 && cell[x][(y + 1) % rozmiar].isWorking == 0 &&
			cell[(x + rozmiar - 1) % rozmiar][y].isWorking == 0 && cell[x][(y + rozmiar - 1) % rozmiar].isWorking == 0)
		{
			cell[x][y].isWorking = 1;
			LeaveCriticalSection(&SekcjaKrytyczna);
			suma = 0;
			suma += cell[x][y].state;
			suma += cell[(x + 1) % rozmiar][y].state;
			suma += cell[x][(y + 1) % rozmiar].state;
			suma += cell[(x + rozmiar - 1) % rozmiar][y].state;
			suma += cell[x][(y + rozmiar - 1) % rozmiar].state;
			suma = suma % 9 + 1;
			cell[x][y].state = suma;
			InvalidateRect(hwnd, NULL, FALSE);
			cell[x][y].isWorking = 0;
			Sleep(800);
		}
		else
		{
			LeaveCriticalSection(&SekcjaKrytyczna);
			//MessageBox(NULL, "ZAJ�TE", "", MB_ICONEXCLAMATION | MB_OK);
			Sleep(75);
		}
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	srand(time(NULL));

	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)6;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = NazwaKlasy;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "B��d: Nie mo�na zarejestrowa� klasy okna!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	// TWORZENIE OKNA
	int horizontal = 0;
	int vertical = 0;
	GetDesktopResolution(horizontal, vertical);
	int width = rozmiar*50+20;
	int height = rozmiar * 50 + 43;

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, NazwaKlasy, "Symulacja uk�adu automat�w kom�rkowych", WS_OVERLAPPEDWINDOW,
		horizontal / 2 - width / 2, vertical / 2 - height / 2, width, height, NULL, NULL, hInstance, NULL);

	if (hwnd == NULL)
	{
		MessageBox(NULL, "B��d: Nie mo�na utworzy� okna!", "Error!", MB_ICONEXCLAMATION);
		return 1;
	}
	InitializeCriticalSection(&SekcjaKrytyczna);
	cell = new Cell*[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		cell[i] = new Cell[rozmiar]();
	}
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			cell[i][j].fill(i, j);
			cell[i][j].handle = CreateWindowEx(0, "BUTTON", "X", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON |
				BS_OWNERDRAW, j * 50, i * 50, 50, 50, hwnd, (HMENU)(i * rozmiar + j), hInstance, NULL);
		}
	}
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			cell[i][j].start_thread();
		}
	}
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	while (GetMessage(&Komunikat, NULL, 0, 0))
	{
		TranslateMessage(&Komunikat);
		DispatchMessage(&Komunikat);
	}
	return Komunikat.wParam;
}

// OBS�UGA ZDARZE�
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		DeleteCriticalSection(&SekcjaKrytyczna);
		PostQuitMessage(0);
		break;
	case WM_DRAWITEM:
	{
		LPDRAWITEMSTRUCT lpDIS = (LPDRAWITEMSTRUCT)lParam;	
			if (cell[wParam / rozmiar][wParam % rozmiar].state == 1)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 0, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 2)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 0, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 3)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 255, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 4)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 0, 255));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 5)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 255, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 6)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 0, 255));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 7)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 255, 255));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 8)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 255, 255));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (cell[wParam / rozmiar][wParam % rozmiar].state == 9)
			{
				SetDCBrushColor(lpDIS->hDC, RGB(128, 0, 128));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
	return TRUE;
	break;
	}
	case WM_KEYDOWN:
	{
		switch ((int)wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
	}
	break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}